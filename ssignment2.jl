### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 2c18e6ee-faa2-4a25-ab1b-cb2b714419c3
begin
	using Pkg;
	Pkg.add("PyCall")
	Pkg.add("PlutoUI")
	Pkg.add("TensorFlow")
	Pkg.add("Plots")
	Pkg.add("Distributions")
	Pkg.add("DataFrames")
	Pkg.add("CSV")
	Pkg.add("StatsBase")
	Pkg.add("PyCall")
	Pkg.add("Images")
	Pkg.add("Colors")
	Pkg.add("HDF5")
	Pkg,add("Flux")
end

# ╔═╡ a979ffd7-6672-4f18-ab2a-ca5fd131d552
begin
	using TensorFlow
	using PlutoUI
	using Flux, Flux.Data.MNIST, Statistics
	using Flux: onehotbatch, onecold, crossentropy, throttle
	using Base.Iterators: repeated, partition
	using Printf, BSON
	using Distributions
	using DataFrames
	import CSV
	import StatsBase
	using PyCall
	using Images
	using Colors
	using HDF5
	using CSV
end

# ╔═╡ a2fc434a-4713-42e8-a55b-e0c2f8f2afd4
train_path = "/home/miracle/Desktop/chest_xray/train"

# ╔═╡ 89c49f94-d3e6-4c64-bf9d-5e805f7a781a
test_path = "/home/miracle/Desktop/chest_xray/test"

# ╔═╡ 9525d3e7-0f08-4593-ab24-eddfafc5b4d1
valid_path = "/home/miracle/Desktop/chest_xray/val"

# ╔═╡ 489f9ebb-bc7e-459a-9d72-e2366c3356a4
begin
	length(valid_path)
	length(test_path)
	length(train_path)
end

# ╔═╡ 53eb64a2-442a-4f5c-94ca-341ec4d42f87
function make_minibatch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:9)
    return (X_batch, Y_batch)
end

# ╔═╡ 55246246-0b25-45b3-8177-877e3f500a93
begin
	function readimages(nimages)
	    img=load("/home/miracle/Desktop/chest_xray/val")
	    nx,ny=size(img)
	    @show nx,ny,nimages
	    images=zeros(RGB{Normed{UInt8,8}},nx,ny,nimages)
	    for i=1:nimages
	        img=load("/home/miracle/Desktop/chest_xray/val")
	        images[:,:,i].=img[:,:]
	    end
	    images
	end
	show
end

# ╔═╡ 643fa4f4-ab77-4e55-b922-73ce17d8d87f
begin
	data = Iterators.repeated((train_path, test_path), 100)
	
	model = Chain(
	  Dense(64, 64, train_path),
	  Dense(64, 32, train_path),
	  Dense(32, 1))
	
	loss(x, y) = Flux.mse(model(x), y)
	ps = Flux.params(model)
	opt = ADAM() 
	evalcb = () -> @show(loss(train_path, test_path))

	Flux.train!(loss, params(model), data, opt, cb = evalcb)
end

# ╔═╡ 44ab87ba-67c3-45cc-9fda-5bf5145dc009
histogram(valid_path, label="Rainfall")

# ╔═╡ 7bd9a268-13b2-4279-aa4a-59dbd64e5b82
begin

	images = Flux.Data.MNIST.images();
	labels = Flux.Data.MNIST.labels();
end

# ╔═╡ 2aed15e1-fce2-438d-bf8e-1c8f46fd5dc1
begin
	using Random
	x=rand(1:60000)
	md"""
	$(images[x])
	# $(labels[x])"""
end

# ╔═╡ 68ea173a-a810-4a6e-8bf9-73824fe35e7f
image_data_generator(
  rescale            = 1/255    ,
  rotation_range     = 5        ,
  width_shift_range  = 0.1      ,
  height_shift_range = 0.05     ,
  shear_range        = 0.1      ,
  zoom_range         = 0.15     ,
  horizontal_flip    = 1     ,
  vertical_flip      = 1    ,
  fill_mode          = "reflect"
)

# ╔═╡ Cell order:
# ╠═2c18e6ee-faa2-4a25-ab1b-cb2b714419c3
# ╠═a979ffd7-6672-4f18-ab2a-ca5fd131d552
# ╠═a2fc434a-4713-42e8-a55b-e0c2f8f2afd4
# ╠═89c49f94-d3e6-4c64-bf9d-5e805f7a781a
# ╠═9525d3e7-0f08-4593-ab24-eddfafc5b4d1
# ╠═489f9ebb-bc7e-459a-9d72-e2366c3356a4
# ╠═53eb64a2-442a-4f5c-94ca-341ec4d42f87
# ╠═55246246-0b25-45b3-8177-877e3f500a93
# ╠═643fa4f4-ab77-4e55-b922-73ce17d8d87f
# ╠═44ab87ba-67c3-45cc-9fda-5bf5145dc009
# ╠═7bd9a268-13b2-4279-aa4a-59dbd64e5b82
# ╠═2aed15e1-fce2-438d-bf8e-1c8f46fd5dc1
# ╠═68ea173a-a810-4a6e-8bf9-73824fe35e7f
